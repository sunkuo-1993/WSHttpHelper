package org.ws.httphelper.builder;

import org.ws.httphelper.model.config.ClientConfig;
import org.ws.httphelper.model.http.ClientType;

public final class ClientConfigBuilder {
    private ClientConfig clientConfig;
    private RequestTemplateBuilder parentBuilder;

    private ClientConfigBuilder() {
        clientConfig = new ClientConfig();
    }

    private ClientConfigBuilder(RequestTemplateBuilder parentBuilder) {
        clientConfig = new ClientConfig();
        this.parentBuilder = parentBuilder;
    }

    public static ClientConfigBuilder builder() {
        return new ClientConfigBuilder();
    }

    protected static ClientConfigBuilder builder(RequestTemplateBuilder parentBuilder) {
        return new ClientConfigBuilder(parentBuilder);
    }

    public ClientConfigBuilder threadNumber(int threadNumber) {
        clientConfig.setThreadNumber(threadNumber);
        return this;
    }

    public ClientConfigBuilder timeout(int timeout) {
        clientConfig.setTimeout(timeout);
        return this;
    }

    public ClientConfigBuilder enableCookie(boolean enableCookie) {
        clientConfig.setEnableCookie(enableCookie);
        return this;
    }

    public ClientConfigBuilder clientType(ClientType clientType) {
        clientConfig.setClientType(clientType);
        return this;
    }

    public ClientConfigBuilder enableSsl(boolean enableSsl) {
        clientConfig.setEnableSsl(enableSsl);
        return this;
    }

    public ClientConfigBuilder followRedirects(boolean followRedirects) {
        clientConfig.setFollowRedirects(followRedirects);
        return this;
    }

    public ClientConfigBuilder privateClient(boolean privateClient) {
        clientConfig.setPrivateClient(privateClient);
        return this;
    }

    public RequestConfigBuilder requestConfig(){
        if(this.parentBuilder == null){
            return null;
        }
        this.parentBuilder.clientConfig(build());
        return this.parentBuilder.buildRequestConfig();
    }

    public RequestTemplateBuilder end(){
        if(this.parentBuilder == null){
            return null;
        }
        this.parentBuilder.clientConfig(build());
        return this.parentBuilder;
    }

    public ClientConfig build() {
        return clientConfig;
    }
}
