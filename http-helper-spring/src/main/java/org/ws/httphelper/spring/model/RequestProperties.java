package org.ws.httphelper.spring.model;

import org.apache.commons.lang3.ArrayUtils;
import org.ws.httphelper.common.Constant;
import org.ws.httphelper.core.pipeline.HttpHandler;
import org.ws.httphelper.model.field.ParamField;
import org.ws.httphelper.model.field.ParseField;
import org.ws.httphelper.model.http.ContentType;
import org.ws.httphelper.model.http.HttpMethod;
import org.ws.httphelper.model.http.ResponseType;

public class RequestProperties {

    // 请求方法
    private HttpMethod method = HttpMethod.GET;
    // 默认的header
    private NameValue[] defaultHeaders = null;
    // 默认的cookies：若header同时存在cookie，覆盖header中的
    private NameValue[] defaultCookers = null;
    // 默认的参数
    private NameValue[] defaultParams = null;
    // 处理pipeline
    private Class<HttpHandler>[] handlers = null;
    //
    private boolean defaultHandler = false;
    // 参数定义
    private ParamField[] paramFields = null;
    // 根目录：可通过相对路径请求，同时若使用绝对路径请求，优先使用传入参数
    private String rootUrl = null;
    // 请求编码
    private String charset = Constant.UTF_8;
    // 请求内容类型，与Body相结合
    private ContentType contentType = null;
    // 响应的类型：能够自动解析，根据相应的内容类型执行相应的解析处理
    private ResponseType responseType = null;
    // 解析的字段定义
    private ParseField[] parseHtmlFields = null;

    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    public NameValue[] getDefaultHeaders() {
        return defaultHeaders;
    }

    public void setDefaultHeaders(NameValue[] defaultHeaders) {
        this.defaultHeaders = ArrayUtils.clone(defaultHeaders);
    }

    public NameValue[] getDefaultCookers() {
        return defaultCookers;
    }

    public void setDefaultCookers(NameValue[] defaultCookers) {
        this.defaultCookers = ArrayUtils.clone(defaultCookers);
    }

    public NameValue[] getDefaultParams() {
        return defaultParams;
    }

    public void setDefaultParams(NameValue[] defaultParams) {
        this.defaultParams = ArrayUtils.clone(defaultParams);
    }

    public Class<HttpHandler>[] getHandlers() {
        return handlers;
    }

    public void setHandlers(Class<HttpHandler>[] handlers) {
        this.handlers = ArrayUtils.clone(handlers);
    }

    public ParamField[] getParamFields() {
        return paramFields;
    }

    public void setParamFields(ParamField[] paramFields) {
        this.paramFields = ArrayUtils.clone(paramFields);
    }

    public String getRootUrl() {
        return rootUrl;
    }

    public void setRootUrl(String rootUrl) {
        this.rootUrl = rootUrl;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }

    public ParseField[] getParseHtmlFields() {
        return parseHtmlFields;
    }

    public void setParseHtmlFields(ParseField[] parseHtmlFields) {
        this.parseHtmlFields = ArrayUtils.clone(parseHtmlFields);
    }

    public boolean isDefaultHandler() {
        return defaultHandler;
    }

    public void setDefaultHandler(boolean defaultHandler) {
        this.defaultHandler = defaultHandler;
    }
}
