package org.ws.httphelper.support.pipeline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.core.pipeline.HandlerOrder;
import org.ws.httphelper.core.pipeline.HttpHandler;
import org.ws.httphelper.core.pipeline.RequestHandler;

import java.util.concurrent.ExecutorService;

/**
 * 默认的HandlerContext。
 * 包含一个handler，根据@HandlerOrder生成order。
 * RequestHandler的order范围为int的低16位,value的范围为低15位：0～0x00007fff，第16位为level标示位置，System优先执行，16位设为0，User后执行，16位设为1。
 * ResponseHandler的order范围为int的高15位,value的范围为高14位：0x00010000～0x3fff0000，第31位为level标示位置，System优先执行，31位设为1，User后执行，31位为0。
 */
public class DefaultHandlerContext extends AbstractHandlerContext {

    private static Logger log = LoggerFactory.getLogger(DefaultHandlerContext.class.getName());

    /**
     * 低15位全部是1，requestHandler的最大值
     */
    private static final int LOW_MAX_VALUE = 0x00007fff;
    /**
     * 第16位为1
     */
    private static final int LOW_USER_FLAG = 0x00008000;
    /**
     * 第31位为1
     */
    private static final int HIGH_SYS_FLAG = 0x40000000;
    /**
     * 低14位全部为1：responseHandler的最大值
     */
    private static final int HIGH_MAX_VALUE = 0x00003fff;
    /**
     * 31位为0，30位全部为1：
     */
    private static final int HIGH_ALL_1_VALUE = 0x3fffffff;
    /**
     * 默认request的值
     */
    private static final int DEFAULT_REQUEST_VALUE = 0x00008000;
    /**
     * 默认response的值
     */
    private static final int DEFAULT_RESPONSE_VALUE = 0x00010000;
    /**
     * 当前handler
     */
    private HttpHandler handler;
    /**
     * 正序排列：sys先执行,request是小的先执行,response是大的先执行
     */
    private int order;

    public DefaultHandlerContext(DefaultHttpPipeline pipeline, ExecutorService executor, HttpHandler handler) {
        super(pipeline, executor);
        this.handler = handler;
        HandlerOrder handlerOrder = this.handler.getClass().getAnnotation(HandlerOrder.class);
        this.order = buildOrder(handlerOrder);
        if(log.isDebugEnabled()){
            log.debug("{} order -> {}",handler.getClass(),this.order);
        }
    }

    /**
     * RequestHandler -> 低16位,System的第16位为0,user第16位为1
     * ResponseHandler -> 高15位,System的第15位为1,user第15位为0
     * @return
     */
    private int buildOrder(HandlerOrder handlerOrder){
        if(handlerOrder != null){
            HandlerOrder.OrderLevel level = handlerOrder.level();
            int value = handlerOrder.value();
            // 在低16位
            if(this.handler instanceof RequestHandler) {
                if(value > LOW_MAX_VALUE){
                    value = LOW_MAX_VALUE;
                }
                // 第16位为0
                if(level == HandlerOrder.OrderLevel.SYSTEM){
                    value &= LOW_MAX_VALUE;
                }
                // 16位为1
                else {
                    value |= LOW_USER_FLAG;
                }
            }
            else {
                // responseHandler的最大值
                if(value > HIGH_MAX_VALUE){
                    value = HIGH_MAX_VALUE;
                }
                // 左移16位：高15位
                value = value << 16;
                // 第31位设为1
                if(level == HandlerOrder.OrderLevel.SYSTEM){
                    value |= HIGH_SYS_FLAG;
                }
                // 第31位设为0
                else {
                    value &= HIGH_ALL_1_VALUE;
                }
            }
            return value;
        }
        else {
            // 默认的order
            if(this.handler instanceof RequestHandler) {
                return DEFAULT_REQUEST_VALUE;
            }
            else {
                return DEFAULT_RESPONSE_VALUE;
            }
        }
    }

    @Override
    public HttpHandler handler() {
        return handler;
    }

    @Override
    public int getOrder() {
        return order;
    }

}
