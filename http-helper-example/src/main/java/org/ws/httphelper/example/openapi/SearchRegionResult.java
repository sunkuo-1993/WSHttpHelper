package org.ws.httphelper.example.openapi;

import org.ws.httphelper.builder.annotation.ResponseEntity;

import java.util.List;

@ResponseEntity
public class SearchRegionResult {

    private int status;

    private String message;

    private String resultType;

    private List<SearchRegionItem> results;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public List<SearchRegionItem> getResults() {
        return results;
    }

    public void setResults(List<SearchRegionItem> results) {
        this.results = results;
    }
}
