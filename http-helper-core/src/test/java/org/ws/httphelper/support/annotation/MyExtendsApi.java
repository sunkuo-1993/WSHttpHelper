package org.ws.httphelper.support.annotation;

import org.ws.httphelper.HttpHelper;
import org.ws.httphelper.builder.annotation.Delete;
import org.ws.httphelper.builder.annotation.Get;
import org.ws.httphelper.builder.annotation.HttpOperation;
import org.ws.httphelper.builder.annotation.HttpRequest;
import org.ws.httphelper.builder.annotation.Post;
import org.ws.httphelper.core.pipeline.HttpPipeline;
import org.ws.httphelper.model.http.ContentType;
import org.ws.httphelper.model.http.ResponseFuture;

import java.util.Map;
import java.util.function.Consumer;

@HttpRequest(rootUrl = "http://127.0.0.1:8888")
@HttpOperation
public interface MyExtendsApi extends HttpHelper {

    @Get("http://127.0.0.1:8888/test")
    ResponseFuture getTest();

    @Get
    ResponseFuture getHtml(String url);

    @Delete(contentType = ContentType.RAW_TEXT)
    ResponseFuture deleteForFuture(String url, String in);

    @Delete(contentType = ContentType.RAW_TEXT)
    ResponseFuture deleteByWebForFuture(String url, String in);

    @Post(contentType = ContentType.RAW_JSON)
    ResponseFuture postJson(String url, Map<String, String> data);

    @Post(contentType = ContentType.RAW_JSON)
    <T> T postJsonToGeneric(String url, Map<String, String> data, Class<T> clazz);

    @Post(contentType = ContentType.RAW_JSON)
    Map postJsonToMap(String url, Map<String, String> data, Class<Map> clazz);

    @Post(contentType = ContentType.RAW_JSON)
    void postJsonCallback(String url, Map<String, String> data, Consumer<ResponseFuture> callback);

    @Post(contentType = ContentType.RAW_JSON)
    String postJsonForBody(String url, Map<String, String> data);

    HttpPipeline pipeline();

    @Get(contentType = ContentType.BINARY)
    byte[] downloadImg(String url);

}
