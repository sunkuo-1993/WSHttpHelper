package org.ws.httphelper.core.parser;

import org.ws.httphelper.exception.ParseException;
import org.ws.httphelper.model.field.ParseField;

import java.util.Collection;

/**
 * 解析HTML数据
 */
public interface HtmlParser<T> {

    /**
     * 执行解析
     * @param html
     * @param parseFields
     * @return
     * @throws ParseException
     */
    T parse(String html, Collection<ParseField> parseFields)throws ParseException;

}
