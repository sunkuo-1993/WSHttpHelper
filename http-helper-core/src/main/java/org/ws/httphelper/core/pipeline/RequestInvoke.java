package org.ws.httphelper.core.pipeline;

import org.ws.httphelper.model.RequestContext;

/**
 * 执行请求处理
 */
public interface RequestInvoke {

    void request(RequestContext requestContext);

}
