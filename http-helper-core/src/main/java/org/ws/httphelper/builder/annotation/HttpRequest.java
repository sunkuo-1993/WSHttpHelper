package org.ws.httphelper.builder.annotation;

import org.ws.httphelper.common.Constant;
import org.ws.httphelper.core.pipeline.HttpHandler;
import org.ws.httphelper.model.http.ContentType;
import org.ws.httphelper.model.http.HttpMethod;
import org.ws.httphelper.model.http.ResponseType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

/**
 * 一个接口的Template
 * 修饰Type时：对于接口下所有的方法都生效
 * 修饰Method时：覆盖Type，只针对该Method生效
 */
@Inherited
@Target({ElementType.TYPE,ElementType.METHOD,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface HttpRequest {

    String rootUrl() default "";

    HttpMethod method() default HttpMethod.GET;

    ContentType contentType() default ContentType.EMPTY;

    ResponseType responseType() default ResponseType.HTML;

    RequestParam[] requestParams() default {};

    // requestParams两个都存在时优先使用requestEntity
    Class requestEntity() default Map.class;

    ResponseField[] responseFields() default {};
    // responseFields两个都存在时优先使用responseEntity
    Class responseEntity() default Map.class;

    Header[] headers() default {};

    String charset() default Constant.UTF_8;

    Class<? extends HttpHandler>[] handlers() default {};

}
