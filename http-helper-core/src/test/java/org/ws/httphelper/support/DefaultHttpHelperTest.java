package org.ws.httphelper.support;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.model.field.ExpressionType;
import org.ws.httphelper.model.field.ParseField;
import org.ws.httphelper.model.field.ParseFieldType;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.model.template.RequestTemplate;
import org.ws.httphelper.servlet.JettyServlet;
import org.ws.httphelper.template.DefaultRequestTemplate;

import java.util.Map;

/** 
* DefaultHttpHelper Tester. 
* 
* @author <Authors name> 
* @since <pre>十一月 15, 2020</pre> 
* @version 1.0 
*/ 
public class DefaultHttpHelperTest {

    private static Logger log = LoggerFactory.getLogger(DefaultHttpHelperTest.class.getName());

    private DefaultHttpHelper okHttpHelper;
    private DefaultHttpHelper webHttpHelper;
    private static JettyServlet jettyServlet = new JettyServlet(8888);

    @Before
    public void before() throws Exception {
        jettyServlet.start();
        RequestTemplate template1 = DefaultRequestTemplate.okHttpGetHtml();

        RequestTemplate template2 = DefaultRequestTemplate.webGetHtml();

        okHttpHelper = new DefaultHttpHelper(template1);
        webHttpHelper = new DefaultHttpHelper(template2);
    }

    /**
    *
    * Method: request(String url)
    *
    */
    @Test
    public void testRequestUrl() throws Exception {
        String url = "http://127.0.0.1:8888";
        ResponseFuture okResponseFuture = okHttpHelper.request(url);
        Assert.assertTrue(okResponseFuture.isSuccess());

        log.info(JSON.toJSONString(okResponseFuture, SerializerFeature.PrettyFormat));

        ResponseFuture webResponseFuture = webHttpHelper.request(url);
        Assert.assertTrue(webResponseFuture.isSuccess());

        log.info(JSON.toJSONString(webResponseFuture, SerializerFeature.PrettyFormat));
    }

    /**
    *
    * Method: request(String url, Object input)
    *
    */
    @Test
    public void testRequestForUrlInput() throws Exception {
        String url = "http://127.0.0.1:8888/{path}";
        Map<String,String> data = Maps.newHashMap();
        data.put("path","search_list.html");
        data.put("tn","monline_3_dg");
        data.put("ie","utf-8");
        data.put("wd","www");

        ResponseFuture okResponseFuture = okHttpHelper.request(url,data);
        Assert.assertTrue(okResponseFuture.isSuccess());

        log.info(JSON.toJSONString(okResponseFuture, SerializerFeature.PrettyFormat));

        ResponseFuture webResponseFuture = webHttpHelper.request(url,data);
        Assert.assertTrue(webResponseFuture.isSuccess());

        log.info(JSON.toJSONString(webResponseFuture, SerializerFeature.PrettyFormat));
    }

    /**
    *
    * Method: request(String url, Object input, Class<T> outputClass)
    *
    */
    @Test
    public void testRequestForUrlInputOutputClass() throws Exception {
        String url = "http://127.0.0.1:8888/{path}";
        Map<String,String> data = Maps.newHashMap();
        data.put("path","search_list.html");
        data.put("tn","monline_3_dg");
        data.put("ie","utf-8");
        data.put("wd","http");

        ParseField xpathItemList = new ParseField("xpathItemList", ParseFieldType.LIST,
                "//div[@id='content_left']/div[contains(@class,'c-container')]", ExpressionType.XPATH);
        xpathItemList.addChildField("title","./h3",ExpressionType.XPATH);
        xpathItemList.addChildField("link","./h3/a/@href",ExpressionType.XPATH);
        xpathItemList.addChildField("date","(\\d{4}年\\d{1,2}月\\d{1,2}日)",ExpressionType.REGEX);
        xpathItemList.addChildField("abstract","./div[contains(@class,'c-abstract')]",ExpressionType.XPATH);

        ParseField cssItemList = new ParseField("cssItemList", ParseFieldType.LIST,
                "#content_left .c-container", ExpressionType.CSS);
        cssItemList.addChildField("title","h3",ExpressionType.CSS);
        cssItemList.addChildField("link","h3>a@href",ExpressionType.CSS);
        cssItemList.addChildField("date","span.newTimeFactor_before_abs",ExpressionType.CSS);
        cssItemList.addChildField("abstract","div.c-abstract",ExpressionType.CSS);

        RequestTemplate template = DefaultRequestTemplate.webGetHtml();

        template.getRequestConfig().addParseHtmlField(xpathItemList);
        template.getRequestConfig().addParseHtmlField(cssItemList);

        DefaultHttpHelper httpHelper = new DefaultHttpHelper(template);

        ResponseFuture webResponseFuture = httpHelper.request(url,data);
        Assert.assertTrue(webResponseFuture.isSuccess());

        log.info(JSON.toJSONString(webResponseFuture, SerializerFeature.PrettyFormat));
    }

} 
