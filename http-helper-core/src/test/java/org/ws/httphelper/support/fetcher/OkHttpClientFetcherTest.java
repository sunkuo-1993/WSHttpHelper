package org.ws.httphelper.support.fetcher;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.builder.ClientConfigBuilder;
import org.ws.httphelper.builder.RequestDataBuilder;
import org.ws.httphelper.model.config.ClientConfig;
import org.ws.httphelper.model.http.ClientType;
import org.ws.httphelper.model.http.ContentType;
import org.ws.httphelper.model.http.HttpMethod;
import org.ws.httphelper.model.http.RequestData;
import org.ws.httphelper.model.http.ResponseData;
import org.ws.httphelper.servlet.JettyServlet;

import java.util.concurrent.CountDownLatch;

/** 
* OkHttpClientFetcher Tester. 
* 
* @author <Authors name> 
* @since <pre>十一月 14, 2020</pre> 
* @version 1.0 
*/ 
public class OkHttpClientFetcherTest {

    private static Logger log = LoggerFactory.getLogger(OkHttpClientFetcherTest.class.getName());

    private OkHttpClientFetcher fetcher;
    private static JettyServlet jettyServlet = new JettyServlet(8888);

    @Before
    public void before() throws Exception {
        ClientConfig clientConfig = ClientConfigBuilder.builder()
                .clientType(ClientType.OK_HTTP_CLIENT)
                .enableCookie(true)
                .enableSsl(true)
                .followRedirects(false)
                .threadNumber(4)
                .timeout(3_000)
                .build();
        fetcher = new OkHttpClientFetcher(clientConfig);
        jettyServlet.start();
    }

    @Test
    public void testFetch() throws Exception {
        RequestData getRequestData = RequestDataBuilder.builder()
                .url("http://127.0.0.1:8888")
                .method(HttpMethod.GET)
                .build();

        ResponseData responseData = fetcher.fetch(getRequestData);
        Assert.assertEquals(responseData.getStatus(),200);

        log.info(JSON.toJSONString(responseData, SerializerFeature.PrettyFormat));
    }

    @Test
    public void testFetchAsync() throws Exception {
        RequestData getRequestData = RequestDataBuilder.builder()
                .url("http://127.0.0.1:8888")
                .method(HttpMethod.GET)
                .build();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        fetcher.fetch(getRequestData,responseFuture -> {
            Assert.assertTrue(responseFuture.isSuccess());
            log.info(JSON.toJSONString(responseFuture, SerializerFeature.PrettyFormat));
            countDownLatch.countDown();
        });
        // 等待回调
        countDownLatch.await();
    }


} 
