package org.ws.httphelper.support.annotation;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.builder.HttpHelperBuilder;
import org.ws.httphelper.builder.annotation.Delete;
import org.ws.httphelper.builder.annotation.Get;
import org.ws.httphelper.builder.annotation.HttpRequest;
import org.ws.httphelper.exception.BuildException;
import org.ws.httphelper.model.config.RequestConfig;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.servlet.JettyServlet;
import org.ws.httphelper.support.pipeline.handler.response.SaveFileHandler;

import java.io.File;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/** 
* AnnBuildUtils Tester. 
* 
* @author <Authors name> 
* @since <pre>十一月 15, 2020</pre> 
* @version 1.0 
*/ 
public class AnnResponseEntityTest {

    private static Logger log = LoggerFactory.getLogger(AnnResponseEntityTest.class.getName());

    private TopBaidu topBaidu;

    @Before
    public void before() throws Exception {
        topBaidu = HttpHelperBuilder.builder().builderByAnn(TopBaidu.class);
    }

    @Test
    public void testResponseEntity()throws Exception{
        TopNews topNews = topBaidu.getTopNews();
        log.info(JSON.toJSONString(topNews, SerializerFeature.PrettyFormat));
    }
} 
