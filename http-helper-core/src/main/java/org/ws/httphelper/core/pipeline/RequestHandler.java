package org.ws.httphelper.core.pipeline;

import org.ws.httphelper.exception.RequestException;
import org.ws.httphelper.model.RequestContext;

/**
 * 请求处理器
 */
public interface RequestHandler extends HttpHandler {

    /**
     * 执行前处理.
     * @param requestContext 请求data
     * @return true:成功;false:失败，结束后续处理;
     * @throws RequestException 前处理异常
     */
    boolean handler(RequestContext requestContext) throws RequestException;
}
