package org.ws.httphelper.support.fetcher;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.ws.httphelper.core.fetcher.HttpFetcher;
import org.ws.httphelper.model.config.ClientConfig;
import org.ws.httphelper.template.DefaultClientConfig;

/** 
* FetcherPool Tester. 
* 
* @author <Authors name> 
* @since <pre>十一月 15, 2020</pre> 
* @version 1.0 
*/ 
public class FetcherPoolTest { 

    @Before
    public void before() throws Exception {
    }

    /**
    *
    * Method: getHttpFetcher(ClientConfig buildClientConfig)
    *
    */
    @Test
    public void testGetHttpFetcher() throws Exception {
        ClientConfig config1 = DefaultClientConfig.okHttpClientConfig(4, 1_000);
        ClientConfig config2 = DefaultClientConfig.webClientConfig(4,1_000);

        HttpFetcher okHttpFetcher = FetcherPool.getInstance().getHttpFetcher(config1);
        HttpFetcher okHttpFetcher2 = FetcherPool.getInstance().getHttpFetcher(config1);

        Assert.assertEquals(okHttpFetcher,okHttpFetcher2);

        HttpFetcher webHttpFetcher = FetcherPool.getInstance().getHttpFetcher(config2);
        HttpFetcher webHttpFetcher2 = FetcherPool.getInstance().getHttpFetcher(config2);

        Assert.assertEquals(webHttpFetcher,webHttpFetcher2);

        ClientConfig privateOkConfig = DefaultClientConfig.okHttpClientConfig(1,1_000,false,true);

        HttpFetcher privateOkHttpFetcher = FetcherPool.getInstance().getHttpFetcher(privateOkConfig);
        HttpFetcher privateOkHttpFetcher2 = FetcherPool.getInstance().getHttpFetcher(privateOkConfig);

        Assert.assertNotEquals(privateOkHttpFetcher,privateOkHttpFetcher2);

        ClientConfig privateWebConfig = DefaultClientConfig.webClientConfig(1,1_000,false,true);

        HttpFetcher privateWebHttpFetcher = FetcherPool.getInstance().getHttpFetcher(privateWebConfig);
        HttpFetcher privateWebHttpFetcher2 = FetcherPool.getInstance().getHttpFetcher(privateWebConfig);

        Assert.assertNotEquals(privateWebHttpFetcher,privateWebHttpFetcher2);
    }


} 
