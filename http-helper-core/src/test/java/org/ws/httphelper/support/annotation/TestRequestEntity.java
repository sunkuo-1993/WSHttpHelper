package org.ws.httphelper.support.annotation;

import org.ws.httphelper.builder.annotation.RequestEntity;
import org.ws.httphelper.builder.annotation.RequestParam;

@RequestEntity
public class TestRequestEntity {

    @RequestParam(required = true,validation = "[\\d\\w]+",defaultValue = "value1")
    private String key1;

    @RequestParam(required = true,validation = "\\w+",defaultValue = "value")
    private String key2;

    @RequestParam(required = true,validation = "\\d+",defaultValue = "1")
    private String key3;

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public String getKey3() {
        return key3;
    }

    public void setKey3(String key3) {
        this.key3 = key3;
    }
}
