package org.ws.httphelper.support.annotation;

import org.ws.httphelper.builder.annotation.Get;
import org.ws.httphelper.builder.annotation.HttpClient;
import org.ws.httphelper.builder.annotation.HttpOperation;
import org.ws.httphelper.model.http.ClientType;

@HttpClient(
        clientType = ClientType.WEB_CLIENT, timeout = 10_000,
        enableSsl = true, enableCookie = true, followRedirects = true,
        privateClient = true
)
@HttpOperation
public interface TopBaidu {

    @Get(
            value = "http://top.baidu.com/buzz?b=1&c=513&fr=topbuzz_b1_c513",
            responseEntity = TopNews.class,
            charset = "GBK"
    )
    TopNews getTopNews();

}
