package org.ws.httphelper.example.builder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.ws.httphelper.HttpHelper;
import org.ws.httphelper.builder.HttpHelperBuilder;
import org.ws.httphelper.builder.RequestTemplateBuilder;
import org.ws.httphelper.core.pipeline.ResponseHandler;
import org.ws.httphelper.model.field.ExpressionType;
import org.ws.httphelper.model.field.ParseField;
import org.ws.httphelper.model.field.ParseFieldType;
import org.ws.httphelper.model.http.ClientType;
import org.ws.httphelper.model.http.HttpMethod;
import org.ws.httphelper.model.template.RequestTemplate;
import org.ws.httphelper.support.pipeline.handler.response.ParseHtmlHandler;
import org.ws.httphelper.support.pipeline.handler.response.ParseResponseTypeHandler;
import org.ws.httphelper.support.pipeline.handler.response.SaveFileHandler;
import org.ws.httphelper.template.DefaultClientConfig;
import org.ws.httphelper.template.DefaultHandlers;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Configuration
public class SearchImageHttpHelperConfig {

    /**
     * 请求并解析网页中图片
     * @return
     */
    @Bean
    public HttpHelper searchImgHttp(){
        RequestTemplate template = RequestTemplateBuilder.builder()
                // 客户端配置
                .buildClientConfig()
                .clientType(ClientType.WEB_CLIENT)
                .enableSsl(true)
                .enableCookie(true)
                .followRedirects(true)
                .threadNumber(4)
                .timeout(10_000)
                .privateClient(false)
                .requestConfig()
                // 添加一个HTML网页解析字段:类型为List,使用CSS解析,
                // 解析表达式为img@src ==> 表示获取网页中全部的图片连接
                .addParseHtmlFields(new ParseField("imageList", ParseFieldType.LIST, "img@src", ExpressionType.CSS))
                .method(HttpMethod.GET)
                .charset("UTF-8")
                // 添加:默认请求前处理器
                .addHandler(DefaultHandlers.requestHandlers())
                // 添加:解析响应内容类型
                .addHandler(new ParseResponseTypeHandler())
                // 添加:根据ParseHtmlFields解析HTML处理器
                .addHandler(new ParseHtmlHandler())
                .end()
                .build();
        // 根据template创建一个HttpHelper对象
        return HttpHelperBuilder.builder().builderByTemplate(template);
    }

    /**
     * 保存图片HttpHelper
     * @return
     * @throws URISyntaxException
     */
    @Bean
    public HttpHelper saveImgHttp() throws URISyntaxException {
        RequestTemplate template = RequestTemplateBuilder.builder()
                // 客户端配置:使用默认OkHttpClient配置
                .clientConfig(DefaultClientConfig.okHttpClientConfig(4,3_000,true))
                .buildRequestConfig()
                .method(HttpMethod.GET)
                .charset("UTF-8")
                // 添加:默认请求前处理器
                .addHandler(DefaultHandlers.requestHandlers())
                // 添加:解析响应内容类型
                .addHandler(new ParseResponseTypeHandler())
                // 添加:保存文件Handler
                .addHandler(saveFileHandler())
                .end()
                .build();
        return HttpHelperBuilder.builder().builderByTemplate(template);
    }

    /**
     * 保存文件Handler
     * @return
     * @throws URISyntaxException
     */
    @Bean
    public ResponseHandler saveFileHandler() throws URISyntaxException {
        Path path = Paths.get(this.getClass().getResource("/").toURI());
        String savePath = path.toString()+"/download";
        SaveFileHandler saveFileHandler = new SaveFileHandler(savePath);
        saveFileHandler.setFollowUrlPath(true);
        log.info("savePath:{}",savePath);
        return saveFileHandler;
    }

}
