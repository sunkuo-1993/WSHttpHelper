package org.ws.httphelper.model.template;

import org.ws.httphelper.model.config.ClientConfig;
import org.ws.httphelper.model.config.RequestConfig;

public class RequestTemplate {

    private ClientConfig clientConfig;

    private RequestConfig requestConfig;

    public ClientConfig getClientConfig() {
        return clientConfig;
    }

    public void setClientConfig(ClientConfig clientConfig) {
        this.clientConfig = clientConfig;
    }

    public RequestConfig getRequestConfig() {
        return requestConfig;
    }

    public void setRequestConfig(RequestConfig requestConfig) {
        this.requestConfig = requestConfig;
    }
}
