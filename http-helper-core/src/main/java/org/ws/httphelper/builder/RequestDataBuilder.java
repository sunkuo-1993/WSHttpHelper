package org.ws.httphelper.builder;

import org.ws.httphelper.model.http.ContentType;
import org.ws.httphelper.model.http.HttpMethod;
import org.ws.httphelper.model.http.RequestData;

import java.util.Map;

public final class RequestDataBuilder {
    private RequestData requestData;

    private RequestDataBuilder() {
        requestData = new RequestData();
    }

    public static RequestDataBuilder builder() {
        return new RequestDataBuilder();
    }

    public RequestDataBuilder url(String url) {
        requestData.setUrl(url);
        return this;
    }

    public RequestDataBuilder method(HttpMethod method) {
        requestData.setMethod(method);
        return this;
    }

    public RequestDataBuilder headers(Map<String, String> headers) {
        requestData.setHeaders(headers);
        return this;
    }

    public RequestDataBuilder contentType(ContentType contentType) {
        requestData.setContentType(contentType);
        return this;
    }

    public RequestDataBuilder body(Object body) {
        requestData.setBody(body);
        return this;
    }

    public RequestDataBuilder charset(String charset) {
        requestData.setCharset(charset);
        return this;
    }

    public RequestData build() {
        return requestData;
    }
}
