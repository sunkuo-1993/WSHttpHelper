package org.ws.httphelper;

import org.ws.httphelper.core.pipeline.HttpPipeline;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.model.template.RequestTemplate;

import java.util.function.Consumer;

/**
 * HttpHelper接口.
 * 包含一个确定的ClientConfig，根据ClientConfig创建对应的Client，创建后无法修改配置。
 * 执行Request请求执行pipeline之前Request前后处理。
 * 可以使用该接口请求具有前后处理相同操作的一组接口。
 */
public interface HttpHelper {

    /**
     * 同步请求：只有URL参数.
     * @param url
     * @return 响应
     */
    ResponseFuture request(String url);

    /**
     * 同步请求：有URL和input参数.
     * @param url
     * @param input 一般为Map或Entity(使用@RequestEntity获得默认值或有效验证扩展功能)
     * @return
     */
    ResponseFuture request(String url, Object input);

    /**
     * 同步请求：
     * @param url
     * @param input
     * @param outputClass 指定响应对象类型：若返回类型为JSON自动解析，若返回为HTML需使用@ResponseEntity指定解析规则
     * @param <T>
     * @return
     */
    <T> ResponseFuture<T> request(String url, Object input, Class<T> outputClass);

    /**
     * 异步请求.
     * @param url
     * @param callback 回调函数. 并行线程数在ClientConfig中配置.
     */
    void requestAsync(String url, Consumer<ResponseFuture<Object>> callback);

    /**
     * 异步请求.
     * @param url
     * @param input
     * @param callback
     */
    void requestAsync(String url, Object input, Consumer<ResponseFuture<Object>> callback);

    /**
     * 异步请求.
     * @param url
     * @param input
     * @param callback
     * @param outputClass
     * @param <T>
     */
    <T> void requestAsync(String url, Object input, Consumer<ResponseFuture<T>> callback, Class<T> outputClass);

    /**
     * 请求前后处理管道.
     * @return
     */
    HttpPipeline pipeline();

    /**
     * 模板
     * @return
     */
    RequestTemplate template();
}
