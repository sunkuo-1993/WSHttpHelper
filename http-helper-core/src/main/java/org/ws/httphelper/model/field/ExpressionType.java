package org.ws.httphelper.model.field;

/**
 * 支持解析的：表达式类型
 */
public enum ExpressionType {
    XPATH,CSS,REGEX
}