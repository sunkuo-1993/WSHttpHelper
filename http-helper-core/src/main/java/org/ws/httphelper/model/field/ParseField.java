package org.ws.httphelper.model.field;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ParseField {

    public static final String CSS_ATTR_SEPARATOR = "@";

    public ParseField(){}

    public ParseField(String fieldName, ParseFieldType fieldType, String parseExpression, ExpressionType expressionType){
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.parseExpression = parseExpression;
        this.expressionType = expressionType;
    }
    // 名称
    private String fieldName = null;
    // 类型
    private ParseFieldType fieldType = ParseFieldType.STRING;
    // 提取表达式
    private String parseExpression = null;
    // 表达式类型
    private ExpressionType expressionType = null;
    // CSS选择器的属性
    private String attribute = null;
    private boolean makeAttribute = false;
    // 下级节点
    private List<ParseField> childFieldInfoList = null;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public ParseFieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(ParseFieldType fieldType) {
        this.fieldType = fieldType;
    }

    public String getParseExpression() {
        makeParseExpression();
        return parseExpression;
    }

    public void setParseExpression(String parseExpression) {
        this.parseExpression = parseExpression;
        makeParseExpression();
    }

    private void makeParseExpression(){
        if(!makeAttribute
            && this.expressionType == ExpressionType.CSS
            && StringUtils.isNotBlank(this.parseExpression)
            && this.parseExpression.contains(CSS_ATTR_SEPARATOR)){

            String[] arr = parseExpression.split(CSS_ATTR_SEPARATOR);
            this.parseExpression = arr[0];
            this.attribute = arr[1];
        }
        makeAttribute = true;
    }

    public List<ParseField> getChildFieldInfoList() {
        return childFieldInfoList;
    }

    public ExpressionType getExpressionType() {
        return expressionType;
    }

    public void setExpressionType(ExpressionType expressionType) {
        this.expressionType = expressionType;
    }

    public String getAttribute() {
        makeParseExpression();
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public ParseField addChildField(ParseField childFieldInfo){
        if(childFieldInfoList == null){
            childFieldInfoList = new ArrayList<>();
        }
        childFieldInfoList.add(childFieldInfo);
        return this;
    }

    public ParseField addChildField(String fieldName, String parseExpression){
        ParseField childFieldInfo = new ParseField(fieldName, ParseFieldType.STRING,parseExpression,this.expressionType);
        addChildField(childFieldInfo);
        return this;
    }

    public ParseField addChildField(String fieldName, String parseExpression, ExpressionType expressionType){
        ParseField childFieldInfo = new ParseField(fieldName, ParseFieldType.STRING,parseExpression,expressionType);
        addChildField(childFieldInfo);
        return this;
    }

    public ParseField addChildField(String fieldName, ParseFieldType fieldType, String parseExpression){
        ParseField childFieldInfo = new ParseField(fieldName,fieldType,parseExpression,this.expressionType);
        addChildField(childFieldInfo);
        return this;
    }

    public ParseField addChildField(String fieldName, ParseFieldType fieldType, String parseExpression, ExpressionType expressionType){
        ParseField childFieldInfo = new ParseField(fieldName,fieldType,parseExpression,expressionType);
        addChildField(childFieldInfo);
        return this;
    }

    /**
     * 是否有下级
     * @return
     */
    public boolean hasChild(){
        return CollectionUtils.isNotEmpty(childFieldInfoList);
    }

}
